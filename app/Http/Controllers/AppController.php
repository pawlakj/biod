<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class AppController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('app');
    }

    public function get(Request $request)
    {
        return User::find(Auth::id());
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'iv' => 'max:32',
            'encrypted' => 'max:1048576',
        ]);

        $user = User::find(Auth::id());
        $user->iv = $request->input('iv');
        $user->encrypted = $request->input('encrypted');
        $user->save();

        return $user;
    }
}
