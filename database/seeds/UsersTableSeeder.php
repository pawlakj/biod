<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->email = 'user@user';
        $user->password = bcrypt('user');
        $user->save();

        $user = new User();
        $user->email = 'user2@user';
        $user->password = bcrypt('user');
        $user->save();
    }
}
