import CryptoJS from 'crypto-js';
export default {

    state: {
        key: null,
        iv: null,
    },

    getKey() {
        return this.state.key;
    },

    setKey(key) {
        this.state.key = key;
    },

    getContent() {
        return new Promise((resolve, reject) => {
            if(this.state.key) {
                axios.get('get')
                    .then(({data}) => {
                        if (data.encrypted && data.iv) {
                            this.state.iv = data.iv;
                            const decrypted = this.decrypt(data.encrypted, this.state.key, this.state.iv);
                            if (decrypted && decrypted.decrypted) {
                                resolve(decrypted.content);
                            }
                            resolve(null);
                        }
                        resolve(false);
                    })
                    .catch(() => {
                        reject();
                    });
            } else {
                reject();
            }
        });
    },

    setContent(content) {
        return new Promise((resolve, reject) => {
            if(this.state.key && this.state.iv) {
                const encrypted = this.encrypt(
                    { decrypted: true, content },
                    this.state.key,
                    this.state.iv
                );
                axios.post('update', {encrypted, iv: this.state.iv})
                    .then(({data}) => {
                        if (data.encrypted === encrypted && data.iv === this.state.iv) {
                            resolve();
                        } else {
                            reject();
                        }
                    })
                    .catch(() => {
                        reject();
                    });
            } else {
                reject();
            }
        });
    },

    new() {
        return new Promise((resolve, reject) => {
            const generated = this.generateKeyAndIv();
            const encrypted = this.encrypt(
                { decrypted: true, content: [] },
                generated.key,
                generated.iv
            )
            axios.post('update', {iv: generated.iv, encrypted})
                .then(({data}) => {
                    if (data.encrypted === encrypted && data.iv === generated.iv) {
                        this.state = generated;
                        resolve(this.state.key.toString());
                    } else {
                        reject();
                    }
                })
                .catch(({response}) => {
                    reject();
                });
        });
    },

    isNotNull() {
        return new Promise((resolve, reject) => {
            axios.get('get')
                .then(({data}) => {
                    if (data.encrypted && data.iv) {
                        this.state.iv = data.iv;
                        resolve(false);
                    } else {
                        resolve(true);
                    }
                })
                .catch(() => {
                    reject();
                });
        });
    },

    decrypt(encrypted, hexKey, iv) {
        const key = CryptoJS.enc.Hex.parse(hexKey);
        const options = {iv: CryptoJS.enc.Hex.parse(iv)}
        try {
            return JSON.parse(CryptoJS.AES.decrypt(encrypted, key, options).toString(CryptoJS.enc.Utf8));
        } catch (e) {
            return null;
        }
    },

    encrypt(container, hexKey, iv) {
        const key = CryptoJS.enc.Hex.parse(hexKey);
        const options = {iv: CryptoJS.enc.Hex.parse(iv)};
        return CryptoJS.AES.encrypt(JSON.stringify(container), key, options).toString();
    },

    generateKeyAndIv(size) {
        size = size || 32; // default 256bit / 32byte key length
        const key = CryptoJS.lib.WordArray.random(size).toString();
        const iv = CryptoJS.lib.WordArray.random(16).toString(); // default 128bit / 16byte initialization vector length
        return {key, iv};
    },
}